import { test, expect, APIResponse } from "@playwright/test";
import { product } from "../utils/entities/Produto";
import { user } from "../utils/entities/Usuario"


test.describe("Produtos", async () => {
    let newUser = user;
    newUser.administrador = "true";
    var loginResponse;

    test.beforeAll("POST criar um novo usuário", async ({ request }) => {
        const response = await request.post("usuarios", {
            headers: {
                'Content-Type': 'application/json',
            },
            data: newUser
        });

        loginResponse = await request.post("login/", {
            headers: {
                Authorization: 'Basic token',
                'Content-Type': 'application/json',
            },
            data: {
                "email": newUser.email,
                "password": newUser.password
            }
        });

        loginResponse = JSON.parse(await loginResponse.text())
    });

    test("POST Executa cadastro de um novo produto", async ({ request }) => {
        const produto = product;

        const response = await request.post("produtos", {
            headers: {
                'Authorization': loginResponse.authorization,
                'Content-Type': 'application/json',
            },
            data: produto
        });

        expect(response.status()).toBe(201);
    });
});