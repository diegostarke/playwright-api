import { test, expect } from "@playwright/test";
import { user } from "../utils/entities/Usuario"


test.describe("Usuários", async () => {
    test("POST Executa cadastro de um novo usuário", async ({ request }) => {
        const usuario = user;
        const response = await request.post("usuarios", {
            headers: {
                'Content-Type': 'application/json',
            },
            data: usuario
        });

        expect(response.status()).toBe(201);
    });
});