import { test, expect } from "@playwright/test";
import { user } from "../utils/entities/Usuario"

test.describe("login/", async () => {
  let newUser = user;

  test.beforeAll("POST criar um novo usuário", async ({ request }) => {
    const response = await request.post("usuarios", {
      headers: {
        'Content-Type': 'application/json',
      },
      data: newUser
    });
  });

  test("POST Executa login na API", async ({ request }) => {
    const response = await request.post("login/", {
      headers: {
        Authorization: 'Basic token',
        'Content-Type': 'application/json',
      },
      data: {
        "email": newUser.email,
        "password": newUser.password
      }
    });

    expect(response.status()).toBe(200);
  });

  test("GET Lista usuarios com id específico", async ({ request }) => {
    const responseList = await request.get("usuarios/");
    const bodyList = await responseList.json();

    const response = await request.get(`usuarios/${bodyList.usuarios[0]._id}`);

    expect(response.status()).toBe(200);
    expect(await response.json()).toEqual(expect.objectContaining({
      nome: bodyList.usuarios[0].nome,
      email: bodyList.usuarios[0].email,
      password: bodyList.usuarios[0].password,
      administrador: bodyList.usuarios[0].administrador,
      _id: bodyList.usuarios[0]._id,
    }))
  });
});