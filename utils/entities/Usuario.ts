import { faker } from '@faker-js/faker';

type Administrator = 'true' | 'false';

interface Usuario {
    nome: string;
    email: string;
    password: string;
    administrador: Administrator;
}

function createRandomUser(): Usuario {
    return {
        nome: faker.person.fullName(),
        email: faker.internet.email(),
        password: faker.internet.password(),
        administrador: faker.helpers.arrayElement(['true', 'false']),
    };
}

export const user = createRandomUser();