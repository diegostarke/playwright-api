import { faker } from '@faker-js/faker';


interface Produto {
    nome: string;
    preco: number;
    descricao: string;
    quantidade: number;
}

function createRandomProduct(): Produto {
    return {
        nome: faker.commerce.product(),
        preco: faker.number.int({ min: 1, max: 100 }),
        descricao: faker.commerce.productDescription(),
        quantidade: faker.number.int({ min: 1, max: 99 }),
    };
}

export const product = createRandomProduct();